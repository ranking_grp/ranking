#--------------
# FUNCTION 1
#--------------

SPACE = " "

def parse_line(line: str) -> dict:
    name, *marks = line.split(SPACE)
    marks_dict = {f'm{i+1}': int(mark) for i, mark in enumerate(marks)}
    return {'name': name, **marks_dict}


def load_data(input_file: str) -> list:
    return[parse_line(line) for line in open(input_file)]

print(load_data(sample_input.txt))
