SPACE = " "

def parse_line(line: str) -> dict:
    name, *marks = line.split(SPACE)
    marks = {f'm{i+1}': int(mark) for i, mark in enumerate(marks)}
    return {name: marks}

def load_data(input_data: str) -> dict:
    data = {}
    for line in input_data.strip().split('\n'):
        parsed_line = parse_line(line)
        data.update(parsed_line)
    return data

# Sample input
sample_input = '''A 12 14 16
B 5 6 7
C 17 20 23
D 2 40 12
E 3 41 13
F 7 8 9
G 4 5 6'''

# Load data
data = load_data(sample_input)

# Example comparison function: Compare values of 'A' with other keys
reference_key = 'A'
reference_values = data[reference_key]

def compare_with_reference(data, reference_key):
    reference_values = data[reference_key]
    results = []
    for key, values in data.items():
        if key != reference_key:
            comparison_result = all(values[mark] > reference_values[mark] for mark in reference_values)
            if comparison_result:
                results.append(key)
    return results

# Get comparison results
comparison_results = compare_with_reference(data, reference_key)

# Print the comparison results in the specified format
result_keys = [reference_key] + comparison_results
result_str = " > ".join(result_keys)

print(result_str)

